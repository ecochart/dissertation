using UnityEngine;
using TMPro;
using System.Globalization;
using UnityEngine.UI;

public enum Signs
{
    dad,
    drink,
    eat,
    family,
    goodbye,
    hello,
    mom,
    please,
    thank_you,
    none
}

public class PredictionsVisualizer : MonoBehaviour
{
    public static PredictionsVisualizer instance;

    private string prediction = "";
    private float predictionConfidence = 0.0f;

    [SerializeField] TextMeshProUGUI currentPredictionTxt;
    [SerializeField] TextMeshProUGUI currentPredictionConfidenceTxt;

    [SerializeField] Image predictionImage;
    [SerializeField] Color defaultColor;
    [SerializeField] Color goodPredictionColor;

    private void Awake()
    {
        if (instance == null) { instance = this; }
        else { Destroy(this); }
    }

    public void Update()
    {
        currentPredictionTxt.text = this.prediction;

        currentPredictionConfidenceTxt.text = this.predictionConfidence.ToString() + "%";

        if (VideoPlayerManager.instance.currentSignVideo == Signs.none)
            return;


        bool goodSign = VideoPlayerManager.instance.currentSignVideo.ToString() == this.prediction.Replace(" ","_"); // we replace the space from "thank you" with an underscore

        predictionImage.color = goodSign ? goodPredictionColor : defaultColor;
    }

    
    public void SetCurrentPrediction(string prediction)
    {
        this.prediction = prediction;
        Debug.Log("New prediction: " + prediction);
    }
    public void SetCurrentPredictionConfidence(string predictionConfidence)
    {
        this.predictionConfidence = float.Parse(predictionConfidence, CultureInfo.InvariantCulture.NumberFormat);
        this.predictionConfidence *= 100;
        Debug.Log("     confidence: " + predictionConfidence);
    }
}
