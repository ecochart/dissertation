using UnityEngine;

public class LookAtTarget : MonoBehaviour
{
    [SerializeField] Transform Target;
    [SerializeField] bool Inversed;

    void LateUpdate()
    {
        if (Inversed)
            transform.LookAt(transform.TransformPoint(-transform.InverseTransformPoint(Target.position)));
        else
            transform.LookAt(Target.position);
    }
}
