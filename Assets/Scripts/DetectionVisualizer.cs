using UnityEngine;
using TMPro;

public class DetectionVisualizer : MonoBehaviour
{
    public static DetectionVisualizer instance;

    [SerializeField] Renderer leftHand;
    [SerializeField] Renderer rightHand;

    [SerializeField] Material defaultMat;
    [SerializeField] Material detectedMat;

    [SerializeField] TextMeshProUGUI detectionTxt;
    private string d_text;
    private bool left_detected = false;
    private bool right_detected = false;

    private void Awake()
    {
        if (instance == null) { instance = this; }
        else 
        {
            Debug.LogWarning("Duplicate of 'DetectionVisualizer' on: " + gameObject.name);
            Destroy(this); 
        }
    }

    void Start()
    {
        leftHand.material = defaultMat;
        rightHand.material = defaultMat;
    }

    private void Update()
    {
        // Updates detection text on the main Thread
        detectionTxt.text = d_text;

        // Updates hands' color on the main Thread
        leftHand.material = left_detected ? detectedMat : defaultMat;
        rightHand.material = right_detected ? detectedMat : defaultMat;
    }

    public void GetDetectionResults(int base2_pose_right_left)
    {
        left_detected = (base2_pose_right_left & 0b001) != 0;
        right_detected = (base2_pose_right_left & 0b010) != 0;
        bool pose  = (base2_pose_right_left & 0b100) != 0;

        // Change text to put on the main canvas
        d_text = "";
        if (left_detected)
            d_text += "left ";
        if (right_detected)
            d_text += "right "; 
        if (pose)
            d_text += "pose";
    }
}