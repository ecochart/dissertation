using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Video;

public class VideoPlayerManager : MonoBehaviour
{
    public static VideoPlayerManager instance { get; private set; }

    [SerializeField] VideoPlayer videoPlayer;
    [SerializeField] List<VideoClip> videoClips;
    [SerializeField] TextMeshProUGUI signText;
    public Signs currentSignVideo = Signs.none;

    private void Awake()
    {
        if (instance == null) { instance = this; }
        else
        {
            Debug.LogWarning("Duplicate of 'VideoPlayerManager' on: " + gameObject.name);
            Destroy(this);
        }
    }

    void Start()
    {
        if (videoClips.Count < 1)
        {
            Debug.LogWarning("No video in the Video Player Manager");
            return;
        }

        videoPlayer.clip = videoClips[0];
    }

    public void PlayVideo(int videoIdx)
    {
        if (videoIdx < 0 || videoIdx >= videoClips.Count)
        {
            Debug.LogWarning("Incorrect video index in the Video Player Manager: " + videoIdx);
            return;
        }

        currentSignVideo = (Signs)videoIdx;

        // Show the sign of the current video on the canvas
        signText.text = currentSignVideo.ToString().Replace("_", " ");

        videoPlayer.clip = videoClips[videoIdx];
        videoPlayer.gameObject.SetActive(true);
        videoPlayer.Play();
    }
}
