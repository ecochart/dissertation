using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.Interaction.Toolkit;

[Serializable]
struct TutorialEvent
{
    public float buttonDelay;
    public List<GameObject> gameObjects;
}

public class Tutorial : MonoBehaviour
{
    // Need to explain:
    // - use of the button;
    // - objectif of the application (sum up);
    // - the meaning of things on screen (predictions, hand detection...)

    [SerializeField] float firstButtonDelay = 10;
    [SerializeField] XRSimpleInteractable button;
    [SerializeField] Renderer buttonRenderer;
    [SerializeField] bool AutomaticForTests = false;
    [SerializeField] List<TutorialEvent> events;
    private int currentEvent = -1;

    private void Start()
    {
        if (AutomaticForTests)
            StartCoroutine(DeactivateButtonForSeconds(firstButtonDelay));
    }

    public void NextTutorialEvent()
    {
        currentEvent++;

        StartCoroutine(DeactivateButtonForSeconds(events[currentEvent].buttonDelay));

        foreach (GameObject go in events[currentEvent].gameObjects)
        {
            // Change the state of the gameObject
            go.SetActive(!go.activeSelf);
        }
    }

    private IEnumerator DeactivateButtonForSeconds(float seconds)
    {
        button.enabled = false;
        buttonRenderer.material.SetInt("_Glow", 0);

        yield return new WaitForSecondsRealtime(seconds);

        if (currentEvent >= events.Count - 1)
            yield break;

        button.enabled = true;
        buttonRenderer.material.SetInt("_Glow", 1);

        if (AutomaticForTests)
        {
            yield return new WaitForSecondsRealtime(2);
            NextTutorialEvent();
        }
    }
}
