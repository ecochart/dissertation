using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using UnityEngine;
using System;
using System.Linq;
using System.Globalization;
using TMPro;

public class TCPServer : MonoBehaviour
{
    public static TCPServer instance;

    private Thread thread;
    private TcpListener server;
    private TcpClient client;
    private NetworkStream stream = null;
    private bool running = false;

    [SerializeField] TextMeshProUGUI IPAddressText;
    [SerializeField] TextMeshProUGUI ConnectionStatusText;
    private bool connected = false; // Cannot change text UI on separate thread

    [SerializeField] private string IPAddressString = "127.0.0.1";
    [SerializeField] private int connectionPort = 25001;

    private void Awake()
    {
        if (instance == null) { instance = this; }
        else { Destroy(this); }
    }

    void Start()
    {
        // Server operates on a separate thread so Unity doesn't freeze waiting for data
        ThreadStart threadStart = new ThreadStart(GetData);
        thread = new Thread(threadStart);
        thread.Start();
    }

    private void Update()
    {
        // Updates server's IP address on the main Thread
        if (IPAddressText.text != IPAddressString)
        {
            IPAddressText.text = IPAddressString;
        }

        // Updates connection status text on the main Thread
        ConnectionStatusText.text = connected ? "Connected" : "Not Connected";
        ConnectionStatusText.color = connected ? Color.green : Color.red;
    }

    private void GetData()
    {
        try
        {
            Debug.Log("Creating TCP/IP server...");
            server = new TcpListener(IPAddress.Any, connectionPort);
            server.Start();

            // Retrive the Name of HOST to get the IP address (we want the IPv4)
            IPAddressString = GetHostIPv4Address().ToString();
            Debug.Log("Server started on: " + IPAddressString + " - " + connectionPort);
            connected = false;

            running = true;

            // Buffer for reading data
            byte[] buffer = new byte[1024];


            while (running)
            {
                Debug.Log("Waiting for connection...");

                // Perform a blocking call to accept requests.
                client = server.AcceptTcpClient();

                Debug.Log("Connected!"); 
                connected = true;

                // Get the NetworkStream used to send and receive data.
                stream = client.GetStream();

                int i;

                while ((i = stream.Read(buffer, 0, buffer.Length)) != 0)
                {
                    if (stream.DataAvailable) Debug.LogWarning("Buffer too small to collect all the data received!");

                    // Translate data bytes to a string using UTF8 convention
                    string raw_data = Encoding.UTF8.GetString(buffer, 0, i);

                    // Use the separator to split the messages
                    foreach (string data in raw_data.Split('|', StringSplitOptions.RemoveEmptyEntries))
                    {
                        // Process the data sent by the client
                        ProcessReceivedData(data);
                    }
                }

                Debug.Log("No more data, disconnected client");
                client.Close();
                connected = false;
            }
        }
        catch (SocketException e)
        {
            Debug.Log("SocketException: " + e);
        }
        finally
        {
            Debug.Log("Server stops");
            server.Stop();
        }
    }

    private IPAddress GetHostIPv4Address()
    {
        // retrive the Name of HOST to get the IP address (we want the IPv4)
        string hostName = Dns.GetHostName();

        // fetch all the IP Addresses
        IPAddress[] AllAddresses = Dns.GetHostEntry(hostName).AddressList;

        // return the first IPv4 local address
        return AllAddresses.FirstOrDefault(a => a.AddressFamily == AddressFamily.InterNetwork);
    }

    #region Data Processing
    /// <summary>
    /// Process the messages received from the client.
    /// </summary>
    /// <param name="receivedData">Received data from client (2 chars for identification + data).</param>
    private void ProcessReceivedData(string receivedData)
    {
        if (receivedData.Length < 2)
        {
            Debug.Log("Data received without key identification: " + receivedData);
            return;
        }

        // Check the two first char of the string (used to identify the message type)
        string key = receivedData.Substring(0, 2);

        switch (key)
        {
            // Delay test: to test the delay between the client and the server
            case "T0":
                DelayTest(receivedData.Substring(2));
                break;

            // Received the last prediction from python script
            case "W0":
                PredictionsVisualizer.instance.SetCurrentPrediction(receivedData.Substring(2));
                break;

            // Received the confidence value of the last prediction from python script
            case "W1":
                PredictionsVisualizer.instance.SetCurrentPredictionConfidence(receivedData.Substring(2));
                break;

            case "D0":
                DetectionVisualizer.instance.GetDetectionResults(int.Parse(receivedData.Substring(2)));
                break;

            default:
                Debug.Log("Data received: " + receivedData + " (key=" + key + ")");
                break;
        }
    }

    /// <summary>
    /// Process the data received for a delay test.
    /// </summary>
    /// <param name="data">If null, just need to acknowledge the reception. Else, it means we received the avg delay.</param>
    private void DelayTest(string data)
    {
        if (data == "")
        {
            byte[] byteData = Encoding.UTF8.GetBytes("T0-ACK");
            stream.Write(byteData, 0, byteData.Length);
        }
        else
        {
            // Float is received as "42.01" instead of "42,01" so we need to translate the number format
            float i_delay = float.Parse(data, CultureInfo.InvariantCulture.NumberFormat);
            Debug.Log("Delay client->server: " + i_delay + "ms");
        }
    }
    #endregion

    #region Properly closing the server
    private void OnDestroy()
    {
        CloseEverything();
    }

    private void OnDisable()
    {
        CloseEverything();
    }

    private void OnApplicationQuit()
    {
        CloseEverything();
    }

    private void CloseEverything()
    {
        running = false;

        if (stream != null) stream.Close();
        if (client != null) client.Close();
        if (server != null) server.Stop();
        if (thread != null) thread.Abort(); // Getting error: "ThreadAbortException: Thread was being aborted."
    }
    #endregion
}

