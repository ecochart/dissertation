import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as anim
import keyboard
from my_functions import get_frame_data

def get_mean_over_sequences(sign, nb_sequences = 30, nb_frames = 10):
    # Initiliase the averages
    left_hand_series_avg = [np.zeros(63) for _ in range(nb_frames)]
    right_hand_series_avg = [np.zeros(63) for _ in range(nb_frames)]
    pose_series_avg = [np.zeros(24) for _ in range(nb_frames)]

    for sequence in range(nb_sequences):
        for frame in range(nb_frames):
            # Get the file path
            file_path = os.path.join('data', sign, str(sequence), str(frame) + ".npy")

            # Get data for this frame
            left_hand, right_hand, pose = get_frame_data(file_path)

            # Add this data to each average
            left_hand_series_avg[frame] += left_hand
            right_hand_series_avg[frame] += right_hand
            pose_series_avg[frame] += pose

    # For each frame, divide the sum by the number of sequences to get the average
    for frame in range(nb_frames):
        left_hand_series_avg[frame] = np.divide(left_hand_series_avg[frame], nb_sequences)
        right_hand_series_avg[frame] = np.divide(right_hand_series_avg[frame], nb_sequences)
        pose_series_avg[frame] = np.divide(pose_series_avg[frame], nb_sequences)
    
    return left_hand_series_avg, right_hand_series_avg, pose_series_avg

def get_sequence(sign, sequence, nb_frame=10):
    left_hand_frames = []
    right_hand_frames = []
    pose_frames = []
    for frame in range(nb_frame):
        file_path = os.path.join('data', sign, str(sequence), str(frame) + ".npy")
        # Get data for this frame
        left_hand, right_hand, pose = get_frame_data(file_path)
        # Add it to the list of frames for this sequence
        left_hand_frames.append(left_hand)
        right_hand_frames.append(right_hand)
        pose_frames.append(pose)
    return left_hand_frames, right_hand_frames, pose_frames

def get_XY_from_single_frame(data):
    XYpoints = []
    for i in range(len(data) // 3):
        # get only x, y values for each landmark
        XYpoints.append([data[i*3], data[i*3 + 1]])
    return np.array(XYpoints)

def update_sequence(i):
    # clear the axis each frame
    ax.clear()
    # get the data for all landmarks
    XY_frame = get_XY_from_single_frame(rh_data[i])
    ax.scatter(1 - XY_frame[:,0], 1 - XY_frame[:,1], s=point_size)
    XY_frame = get_XY_from_single_frame(lh_data[i])
    ax.scatter(1 - XY_frame[:,0], 1 - XY_frame[:,1], s=point_size)
    XY_frame = get_XY_from_single_frame(pose_data[i])
    ax.scatter(1 - XY_frame[:,0], 1 - XY_frame[:,1], s=point_size)
    # reformat things
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_title(f"{SIGN}: {sequence+1}/30, {i+1}/10")
    ax.grid(True, linewidth=0.25)
    ax.axis((0, 1, 0, 1))

def update_average(i):
    # clear the axis each frame
    ax.clear()
    # get the data for all landmarks
    XY_frame = get_XY_from_single_frame(rh_data[i])
    ax.scatter(1 - XY_frame[:,0], 1 - XY_frame[:,1], s=point_size)
    XY_frame = get_XY_from_single_frame(lh_data[i])
    ax.scatter(1 - XY_frame[:,0], 1 - XY_frame[:,1], s=point_size)
    XY_frame = get_XY_from_single_frame(pose_data[i])
    ax.scatter(1 - XY_frame[:,0], 1 - XY_frame[:,1], s=point_size)
    # reformat things
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_title(f"{SIGN}: average, {i+1}/10")
    ax.grid(True, linewidth=0.25)
    ax.axis((0, 1, 0, 1))


# -- Animate the data through the 10 frames --
SIGN = 'hello'
for sequence in range(15, 30):
    # Prepare the figure
    fig, ax = plt.subplots()
    point_size = 6
    # Prepare the data
    lh_data, rh_data, pose_data = get_sequence(SIGN, sequence)
    # Plot the animation
    ani = anim.FuncAnimation(fig, update_sequence, frames=10, interval=30)
    plt.show()
# ---------------------------------------------

"""# -- Animate the data through the 10 frames --
SIGN = 'mom'
# Prepare the figure
fig, ax = plt.subplots()
point_size = 6
# Prepare the data
lh_data, rh_data, pose_data = get_mean_over_sequences(SIGN)
# Plot the animation
ani = anim.FuncAnimation(fig, update_average, frames=10, interval=30)
plt.show()
# ---------------------------------------------"""