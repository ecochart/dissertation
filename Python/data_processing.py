import os
import numpy as np
from itertools import product
from my_functions import get_frame_data

def get_XY(data):
    data_XY = []
    for i in range(len(data) // 3):
        data_XY.append(data[3*i])
        data_XY.append(data[3*i + 1])
    return data_XY

def generate_XY_data():
    # Get path to data folder
    LOAD_PATH = os.path.join('data')
    SAVE_PATH = os.path.join('data_XY')

    # All the signs we want to invert
    #actions = np.array(['not_a_sign'])#'a', 'b', 'hello', 'name'])
    actions = np.array(os.listdir(LOAD_PATH))

    # Set parameters for the loop
    max_sequence = 30
    max_frame = 10

    # Create directories for each action, sequence, and frame in the dataset
    for action, sequence in product(actions, range(max_sequence)):
        try:
            os.makedirs(os.path.join(SAVE_PATH, action, str(sequence)))
        except:
            pass

    # We go through each file of each wanted actions
    for action in actions:
        for sequence in range(max_sequence):
            for frame in range(max_frame):
                load_path = os.path.join(LOAD_PATH, action, str(sequence), str(frame) + ".npy")
                left_hand, right_hand, pose = get_frame_data(load_path)
    
                left_hand_XY = get_XY(left_hand)
                right_hand_XY = get_XY(right_hand)
                pose_XY = get_XY(pose)

                # Save the data with left hand and right hand inverted
                XY_landmarks = np.concatenate([left_hand_XY, right_hand_XY, pose_XY])

                # Save the reordered data
                save_path = os.path.join(SAVE_PATH, action, str(sequence), str(frame) + ".npy")
                try:
                    np.save(save_path, XY_landmarks)
                    print(f"File successfuly saved at \'{save_path}\'.")
                except:
                    print(f"ERROR: Cannot save landmarks at \'{save_path}\'.")
                    break

generate_XY_data()