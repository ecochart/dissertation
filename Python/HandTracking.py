import keyboard
import time
from sys import argv
from tcp_client import TCPClient

def main():
    # Default host IP adress and port values
    host, port = '127.0.0.1', 25001

    # Can be changed with cmd arguments
    if (len(argv) > 1):
        host = argv[1]        
    if (len(argv) > 2):
        port = int(argv[2])

    # Create the client
    client = TCPClient(host, port)

    running = True

    while running:
        """
        if time.time() - buffer_time > time_out:
            buffer_time = time.time()
            Send(sock, data)
        """

        if keyboard.is_pressed('d'):
            client.TestDelay(1000)

        if keyboard.is_pressed('escape'):
            print("Closing the socket")
            client.Close()
            running = False


if __name__ == "__main__":
    main()