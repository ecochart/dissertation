import os
import numpy as np
from itertools import product

# Get path to data folder
PATH = os.path.join('data')

# All the signs we want to invert
actions = np.array(['not_a_sign'])#'a', 'b', 'hello', 'name'])

# Set parameters for the loop
max_sequence = 30
max_frame = 10

# Create directories for each action, sequence, and frame in the dataset
for action, sequence in product(actions, range(max_sequence)):
    try:
        os.makedirs(os.path.join(PATH, "_" + action, str(sequence)))
    except:
        pass

# We go through each file of each wanted actions
for action in actions:
    for sequence in range(max_sequence):
        for frame in range(max_frame):
            load_path = os.path.join(PATH, action, str(sequence), str(frame) + ".npy")
            try:
                frame_landmarks = np.load(load_path)
                #print(frame_landmarks)  
            except:
                print(f"ERROR: file at the path \'{load_path}\' could not be load.")

            # In each file:
            left_hand = frame_landmarks[0:63] # from 0 to 62 = left hand
            right_hand = frame_landmarks[63:126] # from 63 to 125 = right hand
            pose = frame_landmarks[126:150] # from 126 to 149 = pose

            # Invert the x coordinates of the two hands ([0;1] range)
            for i in range((int) (len(left_hand) / 3)):
                left_hand[3*i] = 1 - left_hand[3*i]
                right_hand[3*i] = 1 - right_hand[3*i]

            # Save the data with left hand and right hand inverted
            inverted_landmarks = np.concatenate([right_hand, left_hand, pose])
            
            # Save the reordered data
            save_path = os.path.join(PATH, "_" + action, str(sequence), str(frame) + ".npy")
            try:
                np.save(save_path, inverted_landmarks)
                print(f"File successfuly saved at \'{save_path}\'.")
            except:
                print(f"ERROR: Cannot save landmarks at \'{save_path}\'.")
                break