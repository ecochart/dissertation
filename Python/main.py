"""MIT License

Copyright (c) 2022 Dmitrii Govor

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""

# Import necessary libraries
import numpy as np
import os
import string
import mediapipe as mp
import cv2
from my_functions import *
import keyboard
from keras._tf_keras.keras.models import load_model
# tensorflow.keras.models import load_model
import language_tool_python
from sys import argv
from tcp_client import TCPClient

def main():
    print("Starting")
    # Set the path to the data directory
    PATH = os.path.join('data')

    # Create an array of action labels by listing the contents of the data directory
    actions = np.array(os.listdir(PATH))

    # Load the trained model
    model_name = 'my_model_XY.keras'
    print(f"Loading model: {model_name}...")
    model = load_model(model_name)

    # Minimum value for a prediction to be accepted 
    CONFIDENCE_THRESHOLD = 0.9

    # Create an instance of the grammar correction tool
    tool = language_tool_python.LanguageToolPublicAPI('en-UK')

    # Initialize the lists
    sentence, keypoints, last_prediction, grammar, grammar_result = [], [], [], [], []

    # Initialize the best prediction
    best_prediction = ""
    best_prediction_confidence = 0
    
    # -- Initialize the TCP client --
    # Default host IP adress and port values
    host, port = '127.0.0.1', 25001
    # Can be changed with cmd arguments
    if (len(argv) > 1):
        host = argv[1]        
    if (len(argv) > 2):
        port = int(argv[2])
    # Create the client
    client = TCPClient(host, port)
    # -------------------------------

    # Access the camera and check if the camera is opened successfully
    cap = cv2.VideoCapture(0)
    if not cap.isOpened():
        print("Cannot access camera.")
        exit()

    running = True

    # Create a holistic object for sign prediction
    with mp.solutions.holistic.Holistic(min_detection_confidence=0.75, min_tracking_confidence=0.75) as holistic:
        # Run the loop while the camera is open
        while cap.isOpened():
            # Read a frame from the camera
            _, image = cap.read()

            # -- Image Processing --
            image.flags.writeable = False
            # Convert the image from BGR to RGB
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            # Process the image using the model
            results = holistic.process(image)
            # Set the image back to writeable mode
            image.flags.writeable = True
            # Convert the image back from RGB to BGR
            image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
            # ----------------------

            # Draw the sign landmarks on the image using draw_landmarks function from my_functions.py
            draw_landmarks(image, results)
            # Extract keypoints from the pose landmarks using keypoint_extraction function from my_functions.py
            #keypoints.append(keypoint_extraction(results))
            keypoints.append(keypoint_extraction_XY(results))
            
            # -- Send tracking results to Unity --
            tracking_result  = 0b001 if results.left_hand_landmarks else 0
            tracking_result += 0b010 if results.right_hand_landmarks else 0
            tracking_result += 0b100 if results.pose_landmarks else 0
            client.Send("D0", str(tracking_result))
            # ------------------------------------

            # Check if 10 frames have been accumulated
            if len(keypoints) == 10:
                # Convert keypoints list to a numpy array
                keypoints = np.array(keypoints)
                # Make a prediction on the keypoints using the loaded model
                prediction = model.predict(keypoints[np.newaxis, :, :])
                # Clear the keypoints list for the next set of frames
                keypoints = []
                # Get the best prediction
                best_prediction = actions[np.argmax(prediction)]
                best_prediction_confidence = np.amax(prediction)
                # Send the word through the tcp socket
                client.Send("W0", best_prediction)
                client.Send("W1", str(round(best_prediction_confidence, 4)))

                # Check if the maximum prediction value is above confidence threshold
                if best_prediction_confidence > CONFIDENCE_THRESHOLD:
                    # Check if the predicted sign is different from the previously predicted sign
                    if last_prediction != best_prediction:
                        # Append the predicted sign to the sentence list
                        sentence.append(best_prediction)
                        # Record a new prediction to use it on the next cycle
                        last_prediction = best_prediction

            # Flip the image
            image = cv2.flip(image, 1) # !need to flip the image AFTER the processing, otherwise left hand and right hand are inverted!
            # Display the best prediction for the last 10 frames
            cv2.putText(image, f"{best_prediction}: {round(best_prediction_confidence,4) * 100}%", (30, 30),
                        cv2.FONT_HERSHEY_SIMPLEX, 1, (255*best_prediction_confidence, 255*best_prediction_confidence, 255), 2, cv2.LINE_AA)
                
            # Limit the sentence length to 7 elements to make sure it fits on the screen
            if len(sentence) > 7:
                sentence = sentence[-7:]

            # Reset if the "Spacebar" is pressed
            if keyboard.is_pressed(' '):
                sentence, keypoints, last_prediction, grammar, grammar_result = [], [], [], [], []

            # Check if the list is not empty
            if sentence:
                # Capitalize the first word of the sentence
                sentence[0] = sentence[0].capitalize()

            # Check if the sentence has at least two elements
            if len(sentence) >= 2:
                # Check if the last element of the sentence belongs to the alphabet (lower or upper cases)
                if sentence[-1] in string.ascii_lowercase or sentence[-1] in string.ascii_uppercase:
                    # Check if the second last element of sentence belongs to the alphabet or is a new word
                    if sentence[-2] in string.ascii_lowercase or sentence[-2] in string.ascii_uppercase or (sentence[-2] not in actions and sentence[-2] not in list(x.capitalize() for x in actions)):
                        # Combine last two elements
                        sentence[-1] = sentence[-2] + sentence[-1]
                        sentence.pop(len(sentence) - 2)
                        sentence[-1] = sentence[-1].capitalize()

            # Perform grammar check if "Enter" is pressed
            if keyboard.is_pressed('enter'):
                # Record the words in the sentence list into a single string
                text = ' '.join(sentence)
                # Apply grammar correction tool and extract the corrected result
                grammar_result = tool.correct(text)

            if keyboard.is_pressed('d'):
                client.TestDelay(1000)

            if keyboard.is_pressed('escape'):
                print("Closing the application")
                break

            if grammar_result:
                # Calculate the size of the text to be displayed and the X coordinate for centering the text on the image
                textsize = cv2.getTextSize(grammar_result, cv2.FONT_HERSHEY_SIMPLEX, 1, 2)[0]
                text_X_coord = (image.shape[1] - textsize[0]) // 2

                # Draw the sentence on the image
                cv2.putText(image, grammar_result, (text_X_coord, 470),
                            cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)
            else:
                # Calculate the size of the text to be displayed and the X coordinate for centering the text on the image
                textsize = cv2.getTextSize(' '.join(sentence), cv2.FONT_HERSHEY_SIMPLEX, 1, 2)[0]
                text_X_coord = (image.shape[1] - textsize[0]) // 2

                # Draw the sentence on the image
                cv2.putText(image, ' '.join(sentence), (text_X_coord, 470),
                            cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)

            # Show the image on the display
            cv2.imshow('Camera', image)

            cv2.waitKey(1)

            # Check if the 'Camera' window was closed and break the loop
            if cv2.getWindowProperty('Camera',cv2.WND_PROP_VISIBLE) < 1:
                break

        # Release the camera and close all windows
        cap.release()
        cv2.destroyAllWindows()

        # Close the tcp socket
        client.Close()

        # Shut off the server
        tool.close()

if __name__ == "__main__":
    main()