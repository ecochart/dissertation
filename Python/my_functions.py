"""MIT License

Copyright (c) 2022 Dmitrii Govor

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""

import mediapipe as mp
import cv2
import numpy as np
from mediapipe.framework.formats import landmark_pb2

def draw_landmarks(image, results):
    """
    Draw the landmarks on the image.

    Args:
        image (numpy.ndarray): The input image.
        results: The landmarks detected by Mediapipe.

    Returns:
        None
    """
    #if (results.left_hand_landmarks): print("Left")
    #if (results.right_hand_landmarks): print("Right")

    # Draw landmarks for left hand
    mp.solutions.drawing_utils.draw_landmarks(image, results.left_hand_landmarks, mp.solutions.holistic.HAND_CONNECTIONS)
    # Draw landmarks for right hand
    mp.solutions.drawing_utils.draw_landmarks(image, results.right_hand_landmarks, mp.solutions.holistic.HAND_CONNECTIONS)
    # Draw pose landmarks
    draw_pose_landmarks(image, results)

# Note: We only want to display the pose landmarks used, i.e., mouth, shoulders, elbows and wrists.
# To do so, we replace the other landmarks by empty invisible ones. 
def draw_pose_landmarks(rgb_image, results):
    if (not results.pose_landmarks):
        return
    
    pose_landmarks_list = results.pose_landmarks.landmark
    indexes_displayed = range(9, 17)
    pose_landmarks = landmark_pb2.LandmarkList()

    for i in range(len(pose_landmarks_list)):
        landmark_values = pose_landmarks_list[i]
        if (i in indexes_displayed):
            new_landmark = landmark_pb2.Landmark(x=landmark_values.x, y=landmark_values.y, z=landmark_values.z, visibility=landmark_values.visibility)
        else:
            new_landmark = landmark_pb2.Landmark(x=0, y=0, z=0, visibility=0)
        pose_landmarks.landmark.extend([new_landmark])

    mp.solutions.drawing_utils.draw_landmarks(rgb_image, pose_landmarks, mp.solutions.holistic.POSE_CONNECTIONS)

# -- Change in 'my_functions.py' --
# this function is not working in 'data_collection.py' because even though image is mutable (np.ndarray)
# when we set flags.writeable as False, it turns the array into immutable, thus, we cannot mutate image anymore.
# One solution is to write this code in data_collection directly
# ---------------------------------

#def image_process(image, model):
#    """
#    Process the image and obtain sign landmarks.
#
#    Args:
#        image (numpy.ndarray): The input image.
#        model: The Mediapipe holistic object.
#
#    Returns:
#        results: The processed results containing sign landmarks.
#    """
#    # Set the image to read-only mode
#    image.flags.writeable = False
#    # Convert the image from BGR to RGB
#    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
#    # Process the image using the model
#    results = model.process(image)
#    # Set the image back to writeable mode
#    image.flags.writeable = True
#    # Convert the image back from RGB to BGR
#    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
#    return results

"""def keypoint_extraction(results):
    # Extract the keypoints for the left hand if present, otherwise set to zeros
    lh = np.array([[res.x, res.y, res.z] for res in results.left_hand_landmarks.landmark]).flatten() if results.left_hand_landmarks else np.zeros(63)
    # Extract the keypoints for the right hand if present, otherwise set to zeros
    rh = np.array([[res.x, res.y, res.z] for res in results.right_hand_landmarks.landmark]).flatten() if results.right_hand_landmarks else np.zeros(63)
    # Concatenate the keypoints for both hands
    keypoints = np.concatenate([lh, rh])
    return keypoints"""

def keypoint_extraction(results):
    """
    Extract the keypoints from the sign landmarks.

    Args:
        results: The processed results containing sign landmarks.

    Returns:
        keypoints (numpy.ndarray): The extracted keypoints.
    """
    # Extract the keypoints for the left hand if present, otherwise set to zeros
    lh = np.array([[res.x, res.y, res.z] for res in results.left_hand_landmarks.landmark]).flatten() if results.left_hand_landmarks else np.zeros(63)
    # Extract the keypoints for the right hand if present, otherwise set to zeros
    rh = np.array([[res.x, res.y, res.z] for res in results.right_hand_landmarks.landmark]).flatten() if results.right_hand_landmarks else np.zeros(63)
    # Extract the keypoints for the mouth, shoulders, elbows and wrists if present, otherwise set to zeros
    pose = np.array([[res.x, res.y, res.z] for res in results.pose_landmarks.landmark[9:17]]).flatten() if results.pose_landmarks else np.zeros(24)
    # Concatenate the keypoints for both hands
    keypoints = np.concatenate([lh, rh, pose])
    return keypoints

def keypoint_extraction_XY(results):
    """
    Extract the keypoints from the sign landmarks.

    Args:
        results: The processed results containing sign landmarks.

    Returns:
        keypoints (numpy.ndarray): The extracted keypoints.
    """
    # Extract the keypoints for the left hand if present, otherwise set to zeros
    lh = np.array([[res.x, res.y] for res in results.left_hand_landmarks.landmark]).flatten() if results.left_hand_landmarks else np.zeros(42)
    # Extract the keypoints for the right hand if present, otherwise set to zeros
    rh = np.array([[res.x, res.y] for res in results.right_hand_landmarks.landmark]).flatten() if results.right_hand_landmarks else np.zeros(42)
    # Extract the keypoints for the mouth, shoulders, elbows and wrists if present, otherwise set to zeros
    pose = np.array([[res.x, res.y] for res in results.pose_landmarks.landmark[9:17]]).flatten() if results.pose_landmarks else np.zeros(16)
    # Concatenate the keypoints for both hands
    keypoints = np.concatenate([lh, rh, pose])
    return keypoints

def get_frame_data(file_path):
    try:
        frame_landmarks = np.load(file_path)
        #print(frame_landmarks)  
    except:
        print(f"ERROR: file at the path \'{file_path}\' could not be load.")

    # Organisation of the data in each file:
    left_hand = frame_landmarks[0:63] # from 0 to 62 = left hand
    right_hand = frame_landmarks[63:126] # from 63 to 125 = right hand
    pose = frame_landmarks[126:150] # from 126 to 149 = pose

    return left_hand, right_hand, pose