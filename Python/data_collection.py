"""MIT License

Copyright (c) 2022 Dmitrii Govor

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""

# Import necessary libraries
import os
import numpy as np
import cv2
import mediapipe as mp
from itertools import product
from my_functions import *
import keyboard
import mouse

def main():
    # Define the actions (signs) that will be recorded and stored in the dataset
    actions = np.array([#'hello', 'goodbye', 'please',
                        #'thank you', 'family', 
                        #'mom', 
                        #'dad', 'eat', 'drink',
                        'not_a_sign'])

    # Define the number of sequences and frames to be recorded for each action
    sequences = 30
    frames = 10

    # Set the path where the dataset will be stored
    PATH = os.path.join('data')

    # Create directories for each action, sequence, and frame in the dataset
    for action, sequence in product(actions, range(sequences)):
        try:
            os.makedirs(os.path.join(PATH, action, str(sequence)))
        except:
            pass

    # Access the camera and check if the camera is opened successfully
    cap = cv2.VideoCapture(0)
    if not cap.isOpened():
        print("Cannot access camera.")
        exit()

    # Create a MediaPipe Holistic object for hand tracking and landmark extraction
    with mp.solutions.holistic.Holistic(min_detection_confidence=0.75, min_tracking_confidence=0.75) as holistic:
        # Loop through each action, sequence, and frame to record data
        for action, sequence, frame in product(actions, range(sequences), range(frames)):
            # If it is the first frame of a sequence, wait for the spacebar key press to start recording
            if frame == 0:
                while True:
                    if keyboard.is_pressed('escape'):
                        print("Closing the application")
                        cap.release()
                        cv2.destroyAllWindows()
                        exit()

                    # Start recording a new sample when we press Space or Left button of the mouse
                    if keyboard.is_pressed(' ') or mouse.is_pressed():
                        break
                    _, image = cap.read()

                    # -- Image Processing -- 
                    image.flags.writeable = False
                    # Convert the image from BGR to RGB
                    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                    # Process the image using the model
                    results = holistic.process(image)
                    # Set the image back to writeable mode
                    image.flags.writeable = True
                    # Convert the image back from RGB to BGR
                    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
                    # ----------------------

                    # -- Draw the landmarks --
                    draw_landmarks(image, results)
                    # ------------------------

                    # -- Display text on the image --
                    # Flip the image
                    image = cv2.flip(image, 1) # !need to flip the image AFTER the processing, otherwise left hand and right hand are inverted!
                    cv2.putText(image, 'Recroding data for the "{}". Sequence number {}.'.format(action, sequence),
                                (20,20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,255), 1, cv2.LINE_AA)
                    cv2.putText(image, 'Pause.', (20,400), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 2, cv2.LINE_AA)
                    cv2.putText(image, 'Press "Space" or "Mouse Left" when ready.', (20,450), cv2.FONT_HERSHEY_SIMPLEX, .7, (0,0,255), 2, cv2.LINE_AA)
                    cv2.imshow('Camera', image)
                    cv2.waitKey(1)
                    # -------------------------------

                    # Check if the 'Camera' window was closed and break the loop
                    if cv2.getWindowProperty('Camera',cv2.WND_PROP_VISIBLE) < 1:
                        break
            else:
                # For subsequent frames, directly read the image from the camera
                _, image = cap.read()
                # -- Image Processing --
                image.flags.writeable = False
                # Convert the image from BGR to RGB
                image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                # Process the image using the model
                results = holistic.process(image)
                # Set the image back to writeable mode
                image.flags.writeable = True
                # Convert the image back from RGB to BGR
                image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
                # ----------------------

                # -- Draw the landmarks --
                draw_landmarks(image, results)
                # ------------------------

                # -- Display text on the image --
                image = cv2.flip(image, 1) # !need to flip the image AFTER the processing, otherwise left hand and right hand are inverted!
                cv2.putText(image, 'Recroding data for the "{}". Sequence number {}.'.format(action, sequence),
                            (20,20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,255), 1, cv2.LINE_AA)
                cv2.imshow('Camera', image)
                cv2.waitKey(1)
                # -------------------------------

            # Check if the 'Camera' window was closed and break the loop
            if cv2.getWindowProperty('Camera',cv2.WND_PROP_VISIBLE) < 1:
                 break

            # Extract the landmarks from both hands and save them in arrays
            keypoints = keypoint_extraction(results)
            print(keypoints)
            frame_path = os.path.join(PATH, action, str(sequence), str(frame))
            try:
                np.save(frame_path, keypoints)
            except:
                print("Error while saving keypoints")
                break

        # Release the camera and close any remaining windows
        cap.release()
        cv2.destroyAllWindows()

if __name__ == "__main__":
    main()