import socket
import time
import sys

class TCPClient:

    def __init__(self, host, port):
        self.connected = False
        self.delay_testing = False
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # SOCK_STREAM means TCP socket
        print(f"Searching for a host on:")
        print(f"    IP adress: {host}")
        print(f"    port: {port}")
        try:
            self.sock.connect((host, port))
            self.connected = True
            print("Connected!")
        except:
            print("No tcp server found!")

    def Send(self, key="", o_data="", PRINT=False):
        """
        Sends data through the tcp socket.

        Args:
            key (string): Two chars to describe the type of data sent.
            o_data (string): The string containing the data to be sent.
            PRINT (bool): If true, prints the message sent in the console.
        Returns:
            None
        """
        if not self.connected:
            #print("Not connected to the server!")
            return
        
        if (PRINT): print(f"Sending data :\"{o_data}\"")

        o_data = key + o_data + "|" # Add a separator at the end of the message

        try: 
            self.sock.sendall(o_data.encode("utf-8")) # Send data
        except socket.error as e:
            print(f"Error sending data: {e}")
            self.sock.close()
            sys.exit(1)

    def TestDelay(self, iterations):
        if not self.connected:
            #print("Not connected to the server!")
            return
        
        if self.delay_testing:
            return
        
        print("Starting delay test...")
        self.delay_testing = True
        o_data = "T0"
        delay_avg = 0
        for i in range(iterations):
            time_buffer = time.time()
            try:
                self.sock.sendall(o_data.encode("utf-8"))
                self.sock.recv(1024)
                delay_avg += (time.time() - time_buffer) / 2 # divide by 2 because we only want the duration of client->server communication, not client->server->client
            except socket.error as e:
                print(f"    Error while performing delay test: {e}")
                self.sock.close()
                sys.exit(1)
        delay_avg = round(delay_avg * 1000 / iterations, 4) # compute the average delay in ms
        self.sock.sendall(("T0" + str(delay_avg)).encode("utf-8"))
        print(f"    Delay average over {iterations} messages: {delay_avg}ms")
        self.delay_testing = False

    def Close(self):
        self.sock.close()